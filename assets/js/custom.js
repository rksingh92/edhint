/* $(document).ready(function(){
    $('.hamburger').click(function(){
        $(this).toggleClass('close-icon');
    });
}); */

/* Banner water ripple on hover */

$(document).ready(function() {
	try {
		$('.water-ripple').ripples({
			resolution: 256,
			perturbance: 0.04
		});
	}
	catch (e) {
		$('.error').show().text(e);
	}
});

/* Bubbly Button */
var animateButton = function(e) {

    e.preventDefault;
    //reset animation
    e.target.classList.remove('animate');
    
    e.target.classList.add('animate');
    setTimeout(function(){
      e.target.classList.remove('animate');
    },700);
  };
  
  var bubblyButtons = document.getElementsByClassName("bubbly-button");
  
  for (var i = 0; i < bubblyButtons.length; i++) {
    bubblyButtons[i].addEventListener('click', animateButton, false);
  }

if ($(window).width() < 991) {

  var menu = new MmenuLight(
    document.querySelector( '#mymenu' ),
    'all'
  );

  var navigator = menu.navigation({
    // selectedClass: 'Selected',
    // slidingSubmenus: true,
    // theme: 'dark',
    // title: 'Menu'
  });

  var drawer = menu.offcanvas({
    // position: 'left'
  });

  //	Open the menu.
  document.querySelector( 'a[href="#mymenu"]' )
    .addEventListener( 'click', evnt => {
      evnt.preventDefault();
      drawer.open();
    });
}